<?php

class Api_model extends CI_Model {

    function __construct(){
        parent::__construct();
    }

    function usernameExist($fname, $lname) {

        $this->db->where('f_name', $fname);         
        $this->db->where('l_name', $lname);         
        return $this->db->get('tb_user')->num_rows();
    }

    function emailExist($email) {

        $this->db->where('email', $email);
        return $this->db->get('tb_user')->num_rows();
    }
    
    function register($fname, $lname, $email, $password, $user_type) {

        $this->db->set('f_name', $fname);
        $this->db->set('l_name', $lname);
        $this->db->set('email', $email);        
        $this->db->set('password', $password);
        $this->db->set('user_type', $user_type);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_user');
        return $this->db->insert_id();
    }
    
    function businessExist($name) {

        $this->db->where('name', $name);
        return $this->db->get('tb_business')->num_rows();
    }
    function checkBusiness($email) {
        
        $this->db->where('email', $email);
        $user = $this->db->get('tb_user')->row();
        if ($user->user_type == 1) {
            
            $this->db->where('user_id', $user->id);
            $exist = $this->db->get('tb_business')->num_rows();
            
            if ($exist == 0) {
                
                return 0;
            }            
        }
        
        return 1;
    }
    
    function resetPassword($email, $new_password) {
        
        $this->db->where('email', $email);
        $this->db->set("password", $new_password);
        
        $this->db->update('tb_user');
    } 
    
    function createBusiness($user_id, $name, $description, $contact, $category, $state, $city, $special_offer, $file_url) {
        
        $this->db->set('user_id', $user_id);
        $this->db->set('name', $name);
        $this->db->set('description', $description);
        $this->db->set('contact', $contact);
        $this->db->set('category', $category);
        $this->db->set('state', $state);
        $this->db->set('city', $city);
        $this->db->set('special_offer', $special_offer);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->set('logo_url', $file_url);
        
        $this->db->insert('tb_business');
        return $this->db->insert_id();
    }
    
    function addRecommend($user_id, $city, $content) {
        
        $this->db->set('user_id', $user_id);
        $this->db->set('city', $city);
        $this->db->set('content', $content);
        $this->db->set('reg_date', 'NOW()', false);
        $this->db->insert('tb_recommend');
        $this->db->insert_id();
    } 
    
    function getBusiness($city) {
        
        $result = array();
        $this->db->order_by('reg_date', 'DESC');
        $this->db->where('city', $city);
        $query = $this->db->get('tb_business');
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $business) {
                
                $arr = array('id' => $business->id,
                             'user_id' => $business->user_id,
                             'name' => $business->name,
                             'description' => $business->description,
                             'contact' => $business->contact,
                             'category' => $business->category,
                             'state' => $business->state,
                             'city' => $business->city,
                             'special_offer' => $business->special_offer,
                             'logo_url' => $business->logo_url);
                array_push($result, $arr);
            }
        }
        
        return $result;
    }
    
    function getRecommend($city) {
        
        $result = array();
        $this->db->order_by('reg_date', 'DESC');
        $this->db->where('city', $city);
        $query = $this->db->get('tb_recommend');
        
        if ($query->num_rows() > 0) {
            
            foreach($query->result() as $recommend) {
                
                $user = $this->db->where('id', $recommend->user_id)->get('tb_user')->row();
                
                $arr = array('id' => $recommend->id,
                             'user_id' => $recommend->user_id,
                             'f_name' => $user->f_name,
                             'l_name' => $user->l_name,
                             'content' => $recommend->content);
                array_push($result, $arr);                
            }
        }
        return $result;
    } 
    
    function updateBusiness($id, $name, $description, $contact, $category, $state, $city, $special_offer) {
        
        $this->db->where('id', $id);
        $this->db->set('name', $name);
        $this->db->set('description', $description);
        $this->db->set('contact', $contact);
        $this->db->set('category', $category);
        $this->db->set('state', $state);
        $this->db->set('city', $city);
        $this->db->set('special_offer', $special_offer);
        
        $this->db->update('tb_business');
    }
    
    function updateBusinessLogo($id, $file_url) {
        
        $this->db->where('id', $id);
        $this->db->set('logo_url', $file_url);
        
        $this->db->update('tb_business');
    }
    
    function updateRecommend($id, $content) {
        
        $this->db->where('id', $id);
        $this->db->set('content', $content);
        $this->db->update('tb_recommend');
    } 
    
    function deleteRecommend($id) {
        
        $this->db->where('id', $id);
        $this->db->delete('tb_recommend');
    } 
}
?>
