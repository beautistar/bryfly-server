<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller  {


    function __construct(){

        parent::__construct();
        $this->load->helper('url');
        $this->load->database();
        $this->load->model('api_model');
        $this->load->library('session');     
    }
    
    /**
     * Make json response to the client with result code message
     *
     * @param p_result_code : Result code
     * @param p_result_msg : Result message
     * @param p_result : Result json object
     */

     private function doRespond($p_result_code,  $p_result){

         $p_result['result_code'] = $p_result_code;

         $this->output->set_content_type('application/json')->set_output(json_encode($p_result));
     }

     /**
     * Make json response to the client with success.
     * (result_code = 0, result_msg = "success")
     *
     * @param p_result : Result json object
     */

     private function doRespondSuccess($result){

         $this->doRespond(0, $result);
     }

    /**
     * Url decode.
     *
     * @param p_text : Data to decode
     *
     * @return text : Decoded text
     */

     private function doUrlDecode($p_text){

        $p_text = urldecode($p_text);
        $p_text = str_replace('&#40;', '(', $p_text);
        $p_text = str_replace('&#41;', ')', $p_text);
        $p_text = str_replace('%40', '@', $p_text);
        $p_text = str_replace('%20',' ', $p_text);
        $p_text = trim($p_text);


        return $p_text;
     }
       
     
     public function signup($fname, $lname, $email, $password, $user_type) {

         $result = array();

         $fname = $this->doUrlDecode($fname);
         $lname = $this->doUrlDecode($lname);
         $email = $this->doUrlDecode($email);          
         $password = $this->doUrlDecode($password);

         $usernameExist = $this->api_model->usernameExist($fname, $lname);
         $emailExist = $this->api_model->emailExist($email);

         if($usernameExist > 0) { // username already exist

             $this->doRespond(201, $result);
             return;
         }

         if ($emailExist > 0) {

             $this->doRespond(202, $result);
             return;
         }
         
         if(!function_exists('password_hash'))
             $this->load->helper('password');

         $password_hash = password_hash($password, PASSWORD_BCRYPT);

         $result['id'] = $this->api_model->register($fname, $lname, $email, $password_hash, $user_type);  

         $this->doRespondSuccess($result);   
     }                                       
     
     function login($email, $password) { 
         
         $result = array();
         $email = $this->doUrlDecode($email);
         $password = $this->doUrlDecode($password);
                           
         $is_exist = $this->api_model->emailExist($email);

         if ($is_exist == 0) {
             $this->doRespond(203, $result);  // Unregistered email.
             return;
         }         

         if(!function_exists('password_verify'))
             $this->load->helper('password'); // password_helper.php loading

         $row = $this->db->get_where('tb_user', array('email'=>$email))->row();
         $pwd = $row->password;

         if (!password_verify($password, $pwd)){  // wrong password.

             $this->doRespond(204, $result);
             return;

         } else {
             
             $result['user_info'] = array('idx' => $row->id,
                                          'f_name' => $row->f_name,                                          
                                          'email' => $row->email,
                                          'l_name' => $row->l_name,
                                          'user_type' => $row->user_type                                          
                                          );
             //check if user is business but doesn't create their business.
             $exist_business = $this->api_model->checkBusiness($email);
             
             if ($exist_business == 0) {
                 
                 $this->doRespond(207, $result);
                 return;
             }                                                                   
         }

         $this->doRespondSuccess($result); 
     }
     
     function resetPassword($email, $new_password) {
         
         $result = array();
         
         $email = $this->doUrlDecode($email);
         $new_password = $this->doUrlDecode($new_password);
         
         $is_exist = $this->api_model->emailExist($email);

         if ($is_exist == 0) {
             $this->doRespond(203, $result);  // Unregistered email.
             return;
         }
         
         if(!function_exists('password_hash'))
             $this->load->helper('password');

         $password_hash = password_hash($new_password, PASSWORD_BCRYPT);
         
         $this->api_model->resetPassword($email, $password_hash);
         
         $this->doRespondSuccess($result);
     }
     
     function createBusiness() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $name = $_POST['name'];
         $description = $_POST['description'];
         $contact = $_POST['contact'];
         $category = $_POST['category'];
         $state = $_POST['state'];
         $city = $_POST['city'];
         $special_offer = $_POST['special_offer'];
         
         $is_exist = $this->api_model->businessExist($name);

         if ($is_exist > 0) {
             $this->doRespond(205, $result);  // Unregistered email.
             return;
         }
         
         if(!is_dir("uploadfiles/item/")) {
             mkdir("uploadfiles/item/");
         }
         $upload_path = "uploadfiles/item/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $name.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $id = $this->api_model->createBusiness($user_id, $name, $description, $contact, $category, $state, $city, $special_offer, $file_url);
            $result['id'] = $id;
            $result['file_url'] = $file_url;
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }     
     }
     
     function addRecommend() {
         
         $result = array();
         
         $user_id = $_POST['user_id'];
         $content = $_POST['content'];
         $city = $_POST['city'];
         
         $this->api_model->addRecommend($user_id, $city, $content);
         //$result['recommend_info'] = $this->api_model->getRecommend($city);
         
         $this->doRespondSuccess($result);
     }
     
     function getBusiness() {
         
         $result = array();
         
         $city = $_POST['city'];
         
         $result['business_info'] = $this->api_model->getBusiness($city);
         $result['recommend_info'] = $this->api_model->getRecommend($city);
         
         $this->doRespondSuccess($result);
     }
     
     function updateBusiness() {
         
         $result = array();
         
         $id = $_POST['id'];
         $name = $_POST['name'];
         $description = $_POST['description'];
         $contact = $_POST['contact'];
         $category = $_POST['category'];
         $state = $_POST['state'];
         $city = $_POST['city'];
         $special_offer = $_POST['special_offer'];           

         $this->api_model->updateBusiness($id, $name, $description, $contact, $category, $state, $city, $special_offer);
         $this->doRespondSuccess($result);
        
     }
     
     function updateBusinessLogo() {
         
         $result = array();
         
         $id = $_POST['id'];
         
         if(!is_dir("uploadfiles/item/")) {
             mkdir("uploadfiles/item/");
         }
         $upload_path = "uploadfiles/item/";  

         $cur_time = time();
         
         $dateY = date("Y", $cur_time);
         $dateM = date("m", $cur_time);
         
         if(!is_dir($upload_path."/".$dateY)){
             mkdir($upload_path."/".$dateY);
         }
         if(!is_dir($upload_path."/".$dateY."/".$dateM)){
             mkdir($upload_path."/".$dateY."/".$dateM);
         }
         
         $upload_path .= $dateY."/".$dateM."/";
         $upload_url = base_url().$upload_path;

        // Upload file. 

        $w_uploadConfig = array(
            'upload_path' => $upload_path,
            'upload_url' => $upload_url,
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'max_size' => "100000KB",
            'max_width' => 4000,
            'max_height' => 3000,
            'file_name' => $name.intval(microtime(true) * 10)
        );

        $this->load->library('upload', $w_uploadConfig);

        if ($this->upload->do_upload('file')) {

            $file_url = $w_uploadConfig['upload_url'].$this->upload->file_name;
            $id = $this->api_model->updateBusinessLogo($id, $file_url);
            
            $result['file_url'] = $file_url;
            $this->doRespondSuccess($result);

        } else {

            $this->doRespond(206, $result);// upload fail
            return;
        }
     }   
     
     function updateRecommend() {
         
         $result = array();
         
         $id = $_POST['id'];
         $content = $_POST['content'];
         
         $this->api_model->updateRecommend($id, $content);
         //$result['recommend_info'] = $this->api_model->getRecommend($city);
         
         $this->doRespondSuccess($result);
     }
     
     function deleteRecommend() {
         
         $result = array();
         
         $id = $_POST['id'];
         
         $this->api_model->deleteRecommend($id);
         //$result['recommend_info'] = $this->api_model->getRecommend($city);
         
         $this->doRespondSuccess($result);
     }                          
}

?>
