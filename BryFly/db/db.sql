/*
SQLyog Ultimate v11.3 (64 bit)
MySQL - 5.5.52-0ubuntu0.14.04.1 : Database - bryfly
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bryfly` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci */;

USE `bryfly`;

/*Table structure for table `tb_business` */

DROP TABLE IF EXISTS `tb_business`;

CREATE TABLE `tb_business` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `state` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `special_offer` text NOT NULL,
  `logo_url` varchar(255) NOT NULL,
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Data for the table `tb_business` */

insert  into `tb_business`(`id`,`user_id`,`name`,`description`,`contact`,`category`,`state`,`city`,`special_offer`,`logo_url`,`reg_date`) values (1,2,'Redriver Inn','This is a great hotel and has a very cool restaurant that has great food! A lot of other great things to do in the area.','www.companyhotelwebsite.com\n970-555-6211\n13th St Montrose, CO','Lodging','Alabama','Alabaster KEET','This is a special offer that will allow you to get 50% off on any of any order within our restaurant.','http://35.161.165.155/uploadfiles/item/2017/02/14879646970.png','2017-02-23 07:48:11'),(2,8,'Dogz Best Friendz LLC','\"When it comes to the care of dogs, we\'re a dogs best friend\"','7023335322','Other','Nevada','Las Vegas KLAS (McCarran)','Refer a dog friend get $20 off\nGroup basic obedience training session special ask about it.\n','http://35.161.165.155/uploadfiles/item/2017/06/Dogz_Best_Friendz_LLC14969783414.png','2017-06-09 03:19:01'),(3,13,'ooo','Ooo','Ooo','Aviation','Alabama','Alabaster KEET','Test','http://35.161.165.155/uploadfiles/item/2017/12/ooo15132388701.png','2017-12-14 08:07:50');

/*Table structure for table `tb_recommend` */

DROP TABLE IF EXISTS `tb_recommend`;

CREATE TABLE `tb_recommend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `city` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8;

/*Data for the table `tb_recommend` */

insert  into `tb_recommend`(`id`,`user_id`,`city`,`content`,`reg_date`) values (1,4,'Grand Junction KGJT','Being a longtime local here there is so much to do in this awesome valley! You honestly cannot go wrong with any decision. Some of our favorite things to do include renting a pair of skis from Board & Buckle and hitting up the local ski resort called Powderhorn. Grand Junction has world class mountain bike trails, rent a bike and check out what the locals call \"The Lunch Loop\" trails. Just up the road from there is the Colorado National Monument which has breath taking views and amazing hiking trails. When you work up an appetite check out a local favorite called WW Peppers. Finally when it\'s time to relax check out the many wineries located just to the east in Palisade Colorado and have a glass or two.\n','2017-02-02 09:18:37'),(2,4,'Rifle KRIL','This airport provides service to many cities in the area, including being a relief airport for Aspen and Eagle when the weather turns bad. If you find yourself lucky enough to stay for a few days wonder up to Rifle Falls State Park. Here you will find many hiking trails and just like the name implies huge waterfalls at the center of it all. Just to the North is world renowned rock climbing featured in many rock climbing magazines over the years! Hiking trails will lead you to ice caves that last well into late spring. Favorite restaurants include Thai Chili Bistro, Rib City Grill, and Shooters Grill.','2017-02-02 09:56:38'),(3,5,'Alamosa KALS','Mexican Food\nNinos\nCavillos \n\nItalian food \nBistro Rialto\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n','2017-02-03 15:27:27'),(5,4,'Craig KCAG','A small town quite airport with beautiful views of the mountains that surround it. After you land tie down the airplane for a bit. Walk across the runway to the north, you will see a small gate that will take you into the Yampa Valley golf course. Rent some clubs or bring your own and play a few holes on this beautiful course! After the game get a bite to eat at the Tin Cup Grill. Before you leave visit the friendly guys over at Mountain Spray Co for a fuel order!','2017-02-05 18:03:14'),(6,4,'Montrose KMTJ','This airport is surrounded by beauty, with an awesome backdrop of the San Juan Mountain range located to the south of the field. Many of the peaks are over 14,000 ft. Nestled within these mountains is a town called Ouray, and has been referred to as the \"Switzerland of America\". Visiting the Black Canyon is also a must! In my opinion has better views than the Grand Canyon. Favorite restaurants: Camp Robber and Stone House.','2017-02-06 05:00:04'),(7,4,'Cortez KCEZ','The folks over at Cortez Flying Services are very friendly and a great bunch of guys to talk to! I highly recommend visiting Mesa Verde national park. Favorite restaurants are Shiloh steakhouse and The Farm Bistro.','2017-02-10 02:57:01'),(19,4,'Orlando KMCO(International)','We stayed at the Hyatt just North of the airport. Very nice staff and overall great hotel. Just across the street is a park called Airport Lakes Park. They have great walking paths and an awesome frisbee disk golf course.','2017-02-26 00:52:04'),(20,4,'White Plains KHPN','Enjoyed four nights here downtown at the Crown Plaza. There is a number of awesome restaurants on Mamaroneck Ave. Buffalo Wild Wings has the closest atmosphere out of any of their other locations and is located in a 100+ year old bank!','2017-02-26 01:04:55'),(26,7,'Albuquerque KABQ (Sunport)','If you\'re already close to the inner city there\'s two main options. Downtown and old (down)town. If you have the time I\'d recommend going to old town. Plenty of shops and restaurants to choose from. There\'s a free art museum. And an aquarium and planetarium nearby old town as well. Definitely worth a visit. ','2017-03-08 23:14:09'),(27,4,'Los Angeles KLAX','Sheraton Gateway Hotel is a great hotel to stay at however there are no restaurants near by except for the ones in the Hotel which have good food just expensive and small portions. No complimentary breakfast. Shuttle service is good made it from hotel to gate in about 15 minutes ','2017-03-14 16:00:05'),(28,4,'Redmond KRDM','Visit the Redmond Caves there\'s five in total. Make sure you bring more than the light on your cellphone!  Free to walk around in.','2017-03-14 22:25:15'),(29,4,'Milwaukee KMKE','Vanguard is an awesome little place with so many ingredient combinations of bratwurst you would never think of. Only about 5-10 min drive from the airport.','2017-03-17 00:32:01'),(30,4,'Telluride KTEX','Hike Bridal Veil Falls. San Sophia Overlook has an awesome view of the city and valley.','2017-03-18 13:21:26'),(31,4,'Moab KCNY','The guys at Redtail Aviation have always been great to us every time we visit. Indian Ladder is a rock wall with pieces of wood wedged within and ends on top of the cliff wall. It\'s located just two miles up the road from the McDonald\'s on Kane Creek Rd at Moonflower Canyon entrance.','2017-03-18 15:32:04'),(32,4,'Glenwood Springs KGWS','Iron Mountain Springs at night. Glenwood Canyon has numerous hiking trails.','2017-03-30 02:56:42'),(33,4,'Paonia 7V2','What a great small town airport to fly in and out of! Mike with North Fork Aviation was more than generous and making sure we were not left without. Drove into Hotchkiss and had lunch at Pat\'s Bar & Grill, the burgers were outstanding!','2017-04-04 23:20:05'),(34,4,'Sugar Land KSGR','Global Select has a great very pilot friendly FBO! Ask the desk for the code to get into the crew lounge where they have massage chairs and movie theater. ','2017-04-12 15:16:55'),(35,4,'Kansas City KMCK (Downtown)','Downtown area is very nice, lots of things to do! I recommend watching a movie at the Alamo Drafthouse. Every Thursday night during the summer months catch a live concert at the Power and Light District. ','2017-07-07 02:21:37'),(36,4,'Watkins KFTG','Great restaurant inside the FBO!','2017-07-21 18:10:15'),(37,4,'Harrison KHRO','Airplane broke down here for a few days. The staff at Fly Arkansas were great and very accommodating in making sure we had everything we needed. If you have a rental car I recommend driving up to Branson. We raced go karts at Xtreme Racing Center, the next day they send you a text offer to come back and race again for $9!','2017-07-21 18:17:24'),(38,4,'Amarillo KAMA (Husband)','Great restaurant located near Tac Air with views of departing aircraft. Lots of military here.','2017-07-26 15:51:17'),(39,4,'Los Angeles KLAX','Highly recommend staying at the Hilton Airport, very nice hotel with great views!\n','2017-08-07 03:28:01'),(40,4,'Moab KCNY','Fiesta Mexicana has really good Mexican food and if you show up in uniform you usually get 10-25% off your bill. \n\nMoab diner has a cool old school burger joint vibe great place for burgers and ice cream \n\nThe people at Redtail Aviation are amazing don\'t forget to ask for your free can of mixed nuts after you fuel up you won\'t be disappointed. \n\n\n\n\n','2017-08-13 07:08:19'),(49,4,'Boca Raton KBCT','Lunch at Whale’s Rib in Deerfield Beach, FL.\nPretty good seafood and was featured on Diners, Drive-Ins and Dives. About a 15 min drive from KBCT.','2018-01-16 02:34:46'),(50,4,'Atlanta KPDK (Dekalb-Peachtree)','Takorea is a good place with a mix of Korean-Mexican flavors.','2018-01-17 01:58:00'),(51,4,'Sacramento KSMF (International)','Check out old Sacramento Historic District.','2018-03-07 16:13:54'),(52,4,'Prairie Du Chien KPDC ','Great local FBO and friendly services. \nKickapoo Indian caverns.','2018-03-17 13:13:15'),(53,4,'Concord KCCR','Checkout Briones Regional Park- Hiking, mountain  biking, and horseback riding. Escape the city.','2018-04-21 15:33:41');

/*Table structure for table `tb_user` */

DROP TABLE IF EXISTS `tb_user`;

CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `f_name` varchar(100) NOT NULL,
  `l_name` varchar(100) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `user_type` int(2) NOT NULL DEFAULT '0',
  `reg_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Data for the table `tb_user` */

insert  into `tb_user`(`id`,`f_name`,`l_name`,`email`,`password`,`user_type`,`reg_date`) values (1,'Stanley','John','stanley@gmail.com','$2y$10$9nHMkvJEh6HN4JjL5Zvfve18eh95urEwsd2OD6FkmqcPdeSMQibLO',1,'2017-01-07 04:00:28'),(2,'beauti','star','beautistar327@gmail.com','$2y$10$7LT5CjmkyrKdhrCVhACFNu.VkmrwtEw0y3wICo8V7njJicG7pw56a',1,'2017-01-09 09:31:17'),(4,'John','S','stanley_ent19@yahoo.com','$2y$10$kuQpvy4nBI1oMWCPz303..QFEU1Fl/iFWWgXu0G.8mT8QeRA0frla',0,'2017-02-01 13:08:58'),(5,'Richard','Hughes','rhughes198812@gmail.com','$2y$10$5j2Zey4jjifHhdiDilMy/uMJa7S65OVPHVKDhalmXgZKZ51gsBYgu',0,'2017-02-02 03:25:59'),(6,'second','test','kstan826@gmail.com','$2y$10$1Z4y9YyGXtRQA.byjvJCDug8WG/zmLPNwVf/lALrV0M5GnQCWWL6W',0,'2017-02-03 13:00:07'),(7,'John','sunde','jesunde27@gmail.com','$2y$10$uNJ7Xwn6OLdCe6ItIWW7Tupmi/nbFS1uL14H6qwAPOUPVRFrVDxdK',0,'2017-03-08 23:08:58'),(8,'Erica','Bell','dbfz303@gmail.com','$2y$10$GS.oMtSDSVfdZ3nzHBmiAesY8cWI/wQLsesyyVE7wSbjfOoWXMNCu',1,'2017-06-09 03:16:22'),(12,'BryFly','S','info.bryfly@gmail.com','$2y$10$FZdpHvt24joAkNGlApROUeLi7W4wdIy9ARGDTmSdONFx9TcAfyyfe',0,'2017-12-12 22:36:55'),(16,'J','J','j@gmail.com','$2y$10$bpRlAS8sswFCfhfSPgfUD.1om3WqC7VluM24IJbMg0j6wai5gAtBm',0,'2017-12-18 15:44:57'),(21,'Sarah','Mcdermott','Sarah3789@aol.com','$2y$10$TJmixB0VTG2tbkVvtTIOZuzec/wWAFMJoisGKlMxm7AUR.L.jnR.W',0,'2018-01-09 13:27:08'),(22,'Zach','Lovato','zlovato@Gmail.com','$2y$10$w.LGeM4o/u2icr92VHc46eNI7tMFmYMrQJPm6k4TVU3Ib4yJICgMS',0,'2018-01-10 02:35:27'),(23,'Jon','Mayer','jpmayer07@gmail.com','$2y$10$DBmxVE4oz9U0xlpytBaxs.58Vewkfpemsp39khZtvFEVS/L8oRZY2',0,'2018-01-10 12:23:45'),(24,'Will','A','jonkall@hotmail.com','$2y$10$oEMRBiQINgCXdXVqENqjSuZzCQiZnSXiIAXyhtmpz54PgoNwcVy4G',0,'2018-02-06 18:25:49'),(25,'v','p','londonpv@hotmail.com','$2y$10$.MxygsqI8orwMd.z24JIi.RSbOoPWkggiN73ki9Saj2c23PMuAX0O',0,'2018-04-10 23:58:42');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
